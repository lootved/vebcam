# Vebcam

Virtual webcam feed generator for Linux.

Read feed from webcam, transform it with openCV then create a virtual
video feed that can be consumed by any application.

