#!/bin/bash

[ "${1}" = "-f" ] && rm -rf out

# required by cmake to find CMakeFiles of OpenCV
OpenCV_DIR=${REPOS_DIR}/opencv/build
project=Vebcam

mkdir -p out
cd out || exit 1
[ -f build.ninja ] || cmake -DOpenCV_DIR="${OpenCV_DIR}" -GNinja ../
ninja

[ -z "${1}" ] || ./${project}
