#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <linux/videodev2.h>

#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui_c.h>


int main(int, char** argv) {

  cv::VideoCapture cap(0);
  if (!cap.isOpened() ) {
    std::cout << "ERROR INITIALIZING VIDEO CAPTURE" << std::endl;
    return -1;
  }
  char windowName[] = "Webcam Feed";
  cv::namedWindow(windowName, CV_WINDOW_AUTOSIZE);
  int width = 640;
  int height = 480;
  int vidsendsiz = 0;

  int v4l2lo = open("/dev/video7", O_WRONLY);
  if(v4l2lo < 0) {
    std::cout << "Error opening v4l2l device: " << strerror(errno);
    exit(-2);
  }
  struct v4l2_format v;
  int t;
  v.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
  t = ioctl(v4l2lo, VIDIOC_G_FMT, &v);
  if( t < 0 ) {
    exit(t);
  }

  v.fmt.pix.width = width;
  v.fmt.pix.height = height;
  v.fmt.pix.pixelformat = V4L2_PIX_FMT_RGB24;
  vidsendsiz = width * height * 3;
  v.fmt.pix.sizeimage = vidsendsiz;
  t = ioctl(v4l2lo, VIDIOC_S_FMT, &v);
  if( t < 0 ) {
    exit(t);
  }

  while (1) {
    cv::Mat frame;

    if (!cap.grab()) {
      std::cout << "ERROR READING FRAME FROM CAMERA FEED" << std::endl;
      break;
    }

    cap.retrieve(frame);

    cv::Mat mat = cv::Mat(frame.rows, frame.cols, CV_8UC1);
    cv::imshow(windowName, frame);
    cv::cvtColor(frame, mat, cv::COLOR_BGR2GRAY);

    int size = frame.total() * frame.elemSize();
    size_t written = write(v4l2lo, frame.data, size);
    if (written < 0) {
      std::cout << "Error writing v4l2l device";
      close(v4l2lo);
      return 1;
    }

    #define ESCAPE_KEY 10
    switch (cv::waitKey(ESCAPE_KEY)) {
      case 27:
      return 0;
    }
  }

  return 0;
}
